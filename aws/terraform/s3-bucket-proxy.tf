resource "aws_s3_bucket" "s3bucketproxy" {
  bucket = "${var.aws-deployment-prefix}-s3bucketproxy"
  acl    = "public-read"
  tags {
    Organization = "${var.organization}"
    Project = "${var.project}"
    Environment = "${var.environment}"
  }
}