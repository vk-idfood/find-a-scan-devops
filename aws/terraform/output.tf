output "s3-bucket-storage-url" {
  value = "https://s3.${var.aws-region}.amazonaws.com/${aws_s3_bucket.s3bucketstorage.id}/"
}

output "s3-bucket-storage-arn" {
  value = "${aws_s3_bucket.s3bucketstorage.arn}"
}

output "s3-bucket-storage-name" {
  value = "${aws_s3_bucket.s3bucketstorage.id}"
}


output "s3-bucket-proxy-name" {
  value = "${aws_s3_bucket.s3bucketproxy.id}"
}

output "s3-bucket-proxy-url" {
  value = "https://${aws_s3_bucket.s3bucketproxy.bucket_domain_name}/index.html"
}

output "s3-handler-function-name" {
  value = "${aws_lambda_function.lambda-function.id}"
}

output "es-domain-name" {
  value = "${aws_elasticsearch_domain.elastic-search.domain_name}"
}

output "es-role-arn" {
  value = "${aws_iam_role.es-role.arn}"
}

output "cognito-domain-url" {
  value = "https://${aws_cognito_user_pool_domain.cognito-user-pool-domain.domain}.auth.${var.aws-region}.amazoncognito.com"
}

output "cognito-user-pool-id" {
  value = "${aws_cognito_user_pool.cognito-user-pool.id}"
}

output "cognito-username" {
  value = "${var.cognito-username}"
}

output "cognito-password" {
  value = "${random_string.temporary-password.result}"
}

output "cognito-identity-pool-role-auth-arn" {
  value = "${aws_iam_role.cognito-role-auth.arn}"
}

output "cognito-identity-pool-id" {
  value = "${aws_cognito_identity_pool.cognito_identity_pool.id}"
}

output "cognito-identity-pool-name" {
  value = "${aws_cognito_identity_pool.cognito_identity_pool.identity_pool_name}"
}

output "kibana-endpoint" {
  value = "https://${aws_elasticsearch_domain.elastic-search.kibana_endpoint}"
}

output "kibana-username" {
  value = "${var.cognito-username}"
}

output "kibana-temporary-password" {
  value = "${random_string.temporary-password.result}"
}

output "deployment-prefix" {
  value = "${var.aws-deployment-prefix}"
}
