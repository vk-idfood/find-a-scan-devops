resource "aws_s3_bucket" "s3bucketstorage" {
  bucket = "${var.aws-deployment-prefix}-s3bucketstorage"
  acl    = "private"
  
  tags {
    Organization = "${var.organization}"
    Project = "${var.project}"
    Environment = "${var.environment}"
  }
  
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm  = "aws:kms"
      }
    }
  }
  
  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["HEAD", "GET"]
    allowed_origins = ["*"]
    expose_headers  = ["ETag"]
    max_age_seconds = 3600
  }
  
  versioning {
    enabled = true
  }
}