resource "aws_cognito_user_pool" "cognito-user-pool" {
  name    = "${var.aws-deployment-prefix}-cognitouserpool"  
  schema {
    attribute_data_type = "String"
    name                = "family_name"
    required            = true
    mutable             = true
  }
  schema {
    attribute_data_type = "String"
    name                = "email"
    required            = true
    mutable             = true
  }
  schema {
    attribute_data_type = "String"
    name                = "given_name"
    required            = true
    mutable             = true
  }  
  tags {
    Organization = "${var.organization}"
    Project = "${var.project}"
    Environment = "${var.environment}"
  }
}

resource "random_string" "temporary-password" {
  length = 16
  lower = true
  upper = true
  number = true
  special = true
  keepers = {
    ami_id = "${aws_cognito_user_pool.cognito-user-pool.id}"
  }
}

resource "aws_cognito_user_pool_domain" "cognito-user-pool-domain" {
  domain  = "${var.aws-deployment-prefix}"
  user_pool_id = "${aws_cognito_user_pool.cognito-user-pool.id}"
}
