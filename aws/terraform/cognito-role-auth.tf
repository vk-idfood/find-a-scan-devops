resource "aws_iam_role" "cognito-role-auth" {
  name = "${var.aws-deployment-prefix}-cognitoroleauth"
  path = "/${var.aws-deployment-prefix}/"
  assume_role_policy = <<EOF
{  
 "Version":"2012-10-17",
 "Statement":[  
    {  
       "Effect":"Allow",
       "Principal":{  
          "Federated":"cognito-identity.amazonaws.com"
       },
       "Action":"sts:AssumeRoleWithWebIdentity",
       "Condition":{  
          "StringEquals":{  
             "cognito-identity.amazonaws.com:aud":"${aws_cognito_identity_pool.cognito_identity_pool.id}"
          },
          "ForAnyValue:StringLike":{  
             "cognito-identity.amazonaws.com:amr":"authenticated"
          }
       }
    } 
 ]
}
EOF
}

resource "aws_iam_policy" "cognito-policy-auth" {
  name        = "${var.aws-deployment-prefix}-cognitopolicyauth"
  path        = "/${var.aws-deployment-prefix}/"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "mobileanalytics:PutEvents",
        "cognito-sync:*",
        "cognito-identity:*",
        "es:ESHttpGet"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "cognito-role-auth-attachment" {
  name       = "${var.aws-deployment-prefix}-cognitopolicyauthattachment"
  roles      = ["${aws_iam_role.cognito-role-auth.name}"]
  policy_arn = "${aws_iam_policy.cognito-policy-auth.arn}"
}
