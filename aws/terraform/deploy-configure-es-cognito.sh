echo 'Configuring Elasticsearch Service Cognito integration...' && \
export ES_DOMAIN_NAME=$(./aws/terraform/terraform output es-domain-name) && \
export COGNITO_USER_POOL_ID=$(./aws/terraform/terraform output cognito-user-pool-id) && \
export COGNITO_IDENTITY_POOL_ID=$(./aws/terraform/terraform output cognito-identity-pool-id) && \
export ES_ROLE_ARN=$(./aws/terraform/terraform output es-role-arn) && \
aws es update-elasticsearch-domain-config \
  --domain-name $ES_DOMAIN_NAME \
  --cognito-options Enabled=true,UserPoolId="$COGNITO_USER_POOL_ID",IdentityPoolId="$COGNITO_IDENTITY_POOL_ID",RoleArn="$ES_ROLE_ARN"