resource "aws_lambda_function" "lambda-function" {
  function_name    = "${var.aws-deployment-prefix}-lambdafunction"
  filename         = "./aws/terraform/lambda-function-stub.zip"
  role             = "${aws_iam_role.lambda-role.arn}"
  handler          = "index.handler"
  runtime          = "nodejs8.10"

  environment {
    variables = {
      config = "{  \"log\": {    \"level\": 2  },  \"aws\": {\"bucket\": \"${aws_s3_bucket.s3bucketstorage.id}\"}, \"elasticSearch\": {    \"hosts\": [      \"${aws_elasticsearch_domain.elastic-search.endpoint}\"    ],    \"indexName\": \"find-a-scan\",    \"typeName\": \"_doc\"  }}"
    }
  }
  
  tags {
    Organization = "${var.organization}"
    Project = "${var.project}"
    Environment = "${var.environment}"
  } 
}

//Add permission
resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.lambda-function.arn}"
  principal     = "s3.amazonaws.com"
  source_arn    = "${aws_s3_bucket.s3bucketstorage.arn}"
}

//Add s3 bucket trigger
resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = "${aws_s3_bucket.s3bucketstorage.id}"

  lambda_function {
    lambda_function_arn = "${aws_lambda_function.lambda-function.arn}"
    events              = ["s3:ObjectCreated:*"]
  }
}