# Download and install Terraform script
startFolder=$PWD

cd ./aws/terraform && \
  echo 'Installing system dependencies...' && \
  sudo yum install libyaml python-pip python zip wget -y && \
  echo 'Downloading and extracting Terraform...' && \
  wget https://releases.hashicorp.com/terraform/0.11.7/terraform_0.11.7_linux_amd64.zip && \
  unzip ./terraform_0.11.7_linux_amd64.zip && \
  rm -f ./terraform_0.11.7_linux_amd64.zip && \
  echo 'Verifying installation by initializing Terraform AWS provider...' && \
  ./terraform init && \
  cd $startFolder && \
  echo 'Installing awscli...' && \
  sudo pip install awscli && \
  echo 'Seems like we are done! Please check for errors above.'
