echo 'Configuring Cognito User Pool Client...' && \
export COGNITO_USER_POOL_ID=$(./aws/terraform/terraform output cognito-user-pool-id) && \
export DEPLOYMENT_PREFIX=$(./aws/terraform/terraform output deployment-prefix) && \
export S3_BUCKET_PROXY_NAME=$(./aws/terraform/terraform output s3-bucket-proxy-name) && \
export CALLBACK_URL="$(./aws/terraform/terraform output s3-bucket-proxy-url)" && \
export COGNITO_USER_POOL_DOMAIN_URL=$(./aws/terraform/terraform output cognito-domain-url) && \
export S3_BUCKET_STORAGE_NAME=$(./aws/terraform/terraform output s3-bucket-storage-name) && \
export S3_BUCKET_PROXY_NAME=$(./aws/terraform/terraform output s3-bucket-proxy-name) && \
export COGNITO_IDENTITY_POOL_ID=$(./aws/terraform/terraform output cognito-identity-pool-id) && \
export AWS_CLI_OUTPUT=$(aws cognito-idp create-user-pool-client \
  --user-pool-id "$COGNITO_USER_POOL_ID" \
  --client-name "$DEPLOYMENT_PREFIX-s3cognitoproxy" \
  --no-generate-secret \
  --allowed-o-auth-flows "implicit" \
  --allowed-o-auth-scopes "openid" \
  --allowed-o-auth-flows-user-pool-client \
  --supported-identity-providers "COGNITO" \
  --callback-urls "[\"$CALLBACK_URL\"]") && \
export S3_PROXY_CLIENT_ID=$(echo $AWS_CLI_OUTPUT | ./jq --raw-output '.UserPoolClient.ClientId') && \
export AWS_CLI_OUTPUT=$(aws cognito-identity describe-identity-pool --identity-pool-id $COGNITO_IDENTITY_POOL_ID) && \
export KIBANA_CLIENT_ID=$(echo $AWS_CLI_OUTPUT | ./jq --raw-output '.CognitoIdentityProviders[0].ClientId') && \
export COGNITO_IDENTITY_POOL_NAME=$(./aws/terraform/terraform output cognito-identity-pool-name) && \
export COGNITO_PROVIDER_NAME="cognito-idp.$AWS_DEFAULT_REGION.amazonaws.com/$COGNITO_USER_POOL_ID"  && \
read -r -d '' IDENTITY_POOL_JSON << EOM
{
  "IdentityPoolId": "$COGNITO_IDENTITY_POOL_ID",    
  "AllowUnauthenticatedIdentities": true,
  "CognitoIdentityProviders": [
    {
        "ServerSideTokenCheck": true,
        "ClientId": "$KIBANA_CLIENT_ID",
        "ProviderName": "$COGNITO_PROVIDER_NAME"
    },
    {
        "ServerSideTokenCheck": false,
        "ClientId": "$S3_PROXY_CLIENT_ID",
        "ProviderName": "$COGNITO_PROVIDER_NAME"
    }
  ],
  "IdentityPoolName": "idfood_dev_findascan_cognitoidentitypool"
}
EOM
echo "IDENTITY_POOL_JSON: $IDENTITY_POOL_JSON" && \
aws cognito-identity update-identity-pool --cli-input-json "$IDENTITY_POOL_JSON" >> ./output.txt && \
mkdir -p ./s3-cognito-proxy/js && \
echo "const region            = '$AWS_DEFAULT_REGION';" > ./s3-cognito-proxy/js/settings.js && \
echo "const domainUrl         = '$COGNITO_USER_POOL_DOMAIN_URL'" >> ./s3-cognito-proxy/js/settings.js && \
echo "const userPoolId        = '$COGNITO_USER_POOL_ID';" >> ./s3-cognito-proxy/js/settings.js && \
echo "const bucketName        = '$S3_BUCKET_STORAGE_NAME';" >> ./s3-cognito-proxy/js/settings.js && \
echo "const clientId          = '$S3_PROXY_CLIENT_ID';" >> ./s3-cognito-proxy/js/settings.js && \
echo "const identityPoolId    = '$COGNITO_IDENTITY_POOL_ID';" >> ./s3-cognito-proxy/js/settings.js && \
aws s3api put-object \
  --bucket $S3_BUCKET_PROXY_NAME \
  --key js/settings.js \
  --body ./s3-cognito-proxy/js/settings.js \
  --acl 'public-read'  >> ./output.txt && \
rm -rf ./s3-cognito-proxy 