echo 'Setting up S3 storage bucket policy...' && \
export S3_BUCKET_STORAGE_NAME=$(./aws/terraform/terraform output s3-bucket-storage-name) && \
export S3_BUCKET_STORAGE_ARN=$(./aws/terraform/terraform output s3-bucket-storage-arn) && \
export COGNITO_IDENTITY_POOL_ROLE_AUTH_ARN=$(./aws/terraform/terraform output cognito-identity-pool-role-auth-arn) && \
aws s3api put-bucket-policy \
  --bucket "$S3_BUCKET_STORAGE_NAME" \
  --policy "{\"Version\": \"2012-10-17\", \"Statement\": [{\"Effect\":  \"Allow\", \"Principal\": {\"AWS\": \"$COGNITO_IDENTITY_POOL_ROLE_AUTH_ARN\"},\"Action\": \"s3:GetObject\", \"Resource\": \"$S3_BUCKET_STORAGE_ARN/*\"}] }"