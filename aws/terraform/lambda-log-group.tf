resource "aws_cloudwatch_log_group" "lambda-log-group" {
  name = "/aws/lambda/${var.aws-deployment-prefix}-lambdafunction"
  retention_in_days = 30
  tags {
    Organization = "${var.organization}"
    Project = "${var.project}"
    Environment = "${var.environment}"
  }
}