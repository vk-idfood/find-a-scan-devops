resource "aws_iam_role" "lambda-role" {
  name = "${var.aws-deployment-prefix}-lambdarole"
  path = "/${var.aws-deployment-prefix}/"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "lambda-policy" {
  name        = "${var.aws-deployment-prefix}-lambdapolicy"
  path        = "/${var.aws-deployment-prefix}/"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogStream",
        "logs:DescribeLogStreams",
        "logs:CreateLogGroup",
        "logs:PutLogEvents",
        "kms:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": ["s3:*"],
      "Resource": [
        "${aws_s3_bucket.s3bucketstorage.arn}",
        "${aws_s3_bucket.s3bucketstorage.arn}/*"
      ]
    }   
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "lambda-policy-attachment" {
  name       = "${var.aws-deployment-prefix}-lambdapolicyattachment"
  roles      = ["${aws_iam_role.lambda-role.name}"]
  policy_arn = "${aws_iam_policy.lambda-policy.arn}"
}
