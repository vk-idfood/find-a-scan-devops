echo 'Starting deployment: ' $(date) >> ./output.txt && \
./aws/terraform/terraform init ./aws/terraform/ > ./output.txt && \
./aws/terraform/terraform apply \
  -var "aws-access-key=$1" \
  -var "aws-secret-key=$2" \
  -var "aws-region=$3" \
  -var "aws-deployment-prefix=$4" \
  -var "organization=$5" \
  -var "project=$6" \
  -var "environment=$7" \
  -auto-approve \
  ./aws/terraform/ >> ./output.txt && \
echo 'Taking 30 sec pause to let terraform deployment settle...' >> ./output.txt && \
sleep 30 >> ./output.txt && \ 
export AWS_ACCESS_KEY_ID="$1" && \
export AWS_SECRET_ACCESS_KEY="$2" && \
export AWS_DEFAULT_REGION="$3" && \
./aws/terraform/deploy-lambda-function-code.sh >> ./output.txt && \
./aws/terraform/deploy-configure-es-cognito.sh >> ./output.txt && \
./aws/terraform/deploy-s3-bucket-storage-policy.sh >> ./output.txt && \
./aws/terraform/deploy-s3-bucket-proxy-content.sh >> ./output.txt && \
./aws/terraform/deploy-cognito-user-pool-client.sh >> ./output.txt && \
./aws/terraform/deploy-cognito-user-pool-user.sh >> ./output.txt && \
export KIBANA_ENDPOINT=$(./aws/terraform/terraform output kibana-endpoint) >> ./output.txt && \
echo "Kibana end-point: $KIBANA_ENDPOINT" >> ./output.txt && \
export COGNITO_USERNAME=$(./aws/terraform/terraform output cognito-username) >> ./output.txt && \
echo "Username: $COGNITO_USERNAME" >> ./output.txt && \
export COGNITO_PASSWORD=$(./aws/terraform/terraform output cognito-password) >> ./output.txt && \
echo "Temporary Password: $COGNITO_PASSWORD" >> ./output.txt && \
export S3_PROXY_URL="$(./aws/terraform/terraform output s3-bucket-proxy-url)?s3_key={{value}}" >> ./output.txt && \
echo "S3 Proxy Url for Kibana configuration, field 'key': $S3_PROXY_URL" >> ./output.txt && \
echo "Kibana setup is not likely complete at this point" >> ./output.txt && \
echo "Please check ElasticSearch Domain status using Aws Console" >> ./output.txt && \
echo "If it is in 'processing' state it must switch to 'active' before you can access Kibana" >> ./output.txt && \
echo 'Done! Check for errors and feel free to exit with a ctrl-c' >> ./output.txt

