echo 'Deploying lambda-function code...' && \
wget https://bitbucket.org/vk-idfood/find-a-scan-s3-handler/downloads/find-a-scan-s3handler-latest.zip && \
export S3_HANDLER_FUNCTION_NAME=$(./aws/terraform/terraform output s3-handler-function-name) && \
aws lambda update-function-code \
    --function-name "$S3_HANDLER_FUNCTION_NAME" \
    --zip-file fileb://find-a-scan-s3handler-latest.zip \
    --publish && \
rm ./find-a-scan-s3handler-latest.zip*