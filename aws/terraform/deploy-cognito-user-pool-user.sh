echo 'Creating cognito user...' && \
export COGNITO_USERNAME=$(./aws/terraform/terraform output cognito-username) && \
export COGNITO_TEMP_PASSWORD=$(./aws/terraform/terraform output cognito-password) && \
export COGNITO_USER_POOL_ID=$(./aws/terraform/terraform output cognito-user-pool-id) && \
aws cognito-idp admin-create-user \
  --user-pool-id $COGNITO_USER_POOL_ID \
  --username "$COGNITO_USERNAME" \
  --temporary-password "$COGNITO_TEMP_PASSWORD"