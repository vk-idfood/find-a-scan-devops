//Creds
variable "aws-access-key" {}
variable "aws-secret-key" {}
variable "aws-region" {
  default = "us-east-2"
}

//Naming and tagging
variable "aws-deployment-prefix" {}
variable "organization" {}
variable "project" {}
variable "environment" {
  default = "dev"
}

//ElasticSearch
variable "es-instance-type" {
  default = "t2.small.elasticsearch"
}
variable "es-instance-count" {
  default = 1
}
variable "es-volume-size" {
  default = 16
}
variable "es-encrypt-at-rest" {
  default = "false"
}

//Cognito
variable "cognito-username" {
  default = "vladimir.khazin@icssolutions.ca"
}

