resource "aws_cognito_identity_pool" "cognito_identity_pool" {
  identity_pool_name               = "${replace(var.aws-deployment-prefix, "-", "_")}_cognitoidentitypool"
  allow_unauthenticated_identities = true
}

resource "aws_cognito_identity_pool_roles_attachment" "cognito_identity_pool_roles" {
  identity_pool_id = "${aws_cognito_identity_pool.cognito_identity_pool.id}"

  roles {
    "authenticated" = "${aws_iam_role.cognito-role-auth.arn}"
    "unauthenticated" = "${aws_iam_role.cognito-role-unauth.arn}"
  }
}