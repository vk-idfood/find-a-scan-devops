resource "aws_iam_role" "es-role" {
  name = "${var.aws-deployment-prefix}-esrole"
  path = "/${var.aws-deployment-prefix}/"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "es.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "es-policy" {
  name        = "${var.aws-deployment-prefix}-espolicy"
  path        = "/${var.aws-deployment-prefix}/"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
          "cognito-idp:DescribeUserPool",
          "cognito-idp:CreateUserPoolClient",
          "cognito-idp:DeleteUserPoolClient",
          "cognito-idp:DescribeUserPoolClient",
          "cognito-idp:AdminInitiateAuth",
          "cognito-idp:AdminUserGlobalSignOut",
          "cognito-idp:ListUserPoolClients",
          "cognito-identity:DescribeIdentityPool",
          "cognito-identity:UpdateIdentityPool",
          "cognito-identity:SetIdentityPoolRoles",
          "cognito-identity:GetIdentityPoolRoles"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": "iam:PassRole",
      "Resource": "*",
      "Condition": {
        "StringLike": {
            "iam:PassedToService": "cognito-identity.amazonaws.com"
        }
      }
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "es-policy-attachment" {
  name       = "${var.aws-deployment-prefix}-espolicyattachment"
  roles      = ["${aws_iam_role.es-role.name}"]
  policy_arn = "${aws_iam_policy.es-policy.arn}"
}

