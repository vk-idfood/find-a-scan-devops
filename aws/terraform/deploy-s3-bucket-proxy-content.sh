echo 'Deploying static web site for s3 proxy...' && \
rm -rf ./s3-cognito-proxy
export S3_BUCKET_PROXY_NAME=$(./aws/terraform/terraform output s3-bucket-proxy-name) && \
git clone https://bitbucket.org/vk-idfood/find-a-scan-s3-cognito-proxy.git ./s3-cognito-proxy && \
aws s3 sync "./s3-cognito-proxy" "s3://$S3_BUCKET_PROXY_NAME" \
  --acl "public-read" \
  --exclude ".git/**" \
  --exclude ".git*" && \
rm -rf ./s3-cognito-proxy