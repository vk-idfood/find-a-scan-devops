resource "aws_iam_role" "cognito-role-unauth" {
  name = "${var.aws-deployment-prefix}-cognitoroleunauth"
  path = "/${var.aws-deployment-prefix}/"
  assume_role_policy = <<EOF
{  
 "Version":"2012-10-17",
 "Statement":[  
    {  
       "Effect":"Allow",
       "Principal":{  
          "Federated":"cognito-identity.amazonaws.com"
       },
       "Action":"sts:AssumeRoleWithWebIdentity",
       "Condition":{  
          "StringEquals":{  
             "cognito-identity.amazonaws.com:aud":"${aws_cognito_identity_pool.cognito_identity_pool.id}"
          },
          "ForAnyValue:StringLike":{  
             "cognito-identity.amazonaws.com:amr":"unauthenticated"
          }
       }
    }
 ]
}
EOF
}

resource "aws_iam_policy" "cognito-policy-unauth" {
  name        = "${var.aws-deployment-prefix}-cognitopolicyunauth"
  path        = "/${var.aws-deployment-prefix}/"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
          "mobileanalytics:PutEvents",
          "cognito-sync:*",
          "es:ESHttpGet"
      ],
      "Resource": [
          "*"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "cognito-role-unauth-attachment" {
  name       = "${var.aws-deployment-prefix}-cognitopolicyunauthattachment"
  roles      = ["${aws_iam_role.cognito-role-unauth.name}"]
  policy_arn = "${aws_iam_policy.cognito-policy-unauth.arn}"
}
