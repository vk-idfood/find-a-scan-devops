resource "aws_elasticsearch_domain" "elastic-search" {
  domain_name           = "${var.aws-deployment-prefix}-es"
  elasticsearch_version = "6.2"
  encrypt_at_rest {
    enabled = "${var.es-encrypt-at-rest}"
  }
  cluster_config {
    instance_count  = "${var.es-instance-count}"
    instance_type   = "${var.es-instance-type}"
  }
  ebs_options = {
    ebs_enabled = true
    volume_size = "${var.es-volume-size}"
  }
  advanced_options {
    "rest.action.multi.allow_explicit_index" = "true"
  }
  access_policies = <<CONFIG
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "${aws_iam_role.cognito-role-auth.arn}"
      },
      "Action": "es:*"
    }, 
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::231917945460:user/idfood-dev-findascan-user"
      },
      "Action": "es:*"
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "${aws_iam_role.lambda-role.arn}"
      },
      "Action": "es:*"
    },
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "es.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }    
  ]
}
CONFIG

  snapshot_options {
    automated_snapshot_start_hour = 23
  }

  tags {
    Organization = "${var.organization}"
    Project = "${var.project}"
    Environment = "${var.environment}"
  }
}