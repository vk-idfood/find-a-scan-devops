# Find-a-scan DevOps

## AWS

### Artifacts

* Create S3 bucket for storage
* Create ElasticSearch service
* Create Lambda Role
* Grant Lambda role permission to the bucket
* Grant Lambda role permission to the ElasticSearch service
* Create Lambda Function
* Create s3 Lambda Trigger
* Deploy Lambda function code (outstanding)
* Setup Lambda function env variable
* Create Cognito user pool
* Create first Cognito user
* Grant access to Kibana for first Cognito user
* Deploy static web site to another S3 bucket to proxy access to files using Cognito credentials

### Automation

* Installing Terraform:
```
 chmod +x ./aws/terraform/*.sh && \
 chmod +x ./jq && \
  ./aws/terraform/setup.sh
```
* Deploying:

```
./aws/terraform/deploy.sh \
  'aws-key' \
  'aws-secret' \
  'aws-region' \
  'name prefix for deployed assets' \
  'organization name, will be added as a tag whenever supported by aws' \
  'project name, will be added as a tag whenever supported by aws' \
  'environment name, will be added as a tag whenever supported by aws' \
```

### Post Deployment Steps

* A first image needs to be uploaded to the S3 storage bucket to index first document into ElasticSearch
* You can use AWS Web UI, aws cli s3 sync process, WinScp, or any other method for uploading the images to the bucket
* After first image has been uploaded to S3 bucket and indexed into the Elasticsearch
* Kibana index pattern needs to be configured: https://www.elastic.co/guide/en/kibana/current/index-patterns.html
* `key` field formatting needs to be changed to use `Url` rather than default `String` by selecting edit button far right to the field name
* In the `Url template` paste the template printed out from the deployment, e.g.:
`S3 Proxy Url for Kibana configuration, field 'key': https://idfood-dev-findascan-s3bucketproxy.s3.amazonaws.com/index.html?s3_key={{value}}`
* Now you should be all set!
